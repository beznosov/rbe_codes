program main
  USE FFT
  !
  IMPLICIT NONE
  INTEGER, PARAMETER :: nthetah = 101 ! Half of the discretization points 
  INTEGER, PARAMETER :: nrh     = 100 ! Half of the discretization points 
  INTEGER, PARAMETER :: ntheta = 2*nthetah
  INTEGER, PARAMETER :: nr     = 2*nrh
  double precision,  parameter :: pi = 4.D0*DATAN(1.D0)
  double precision,  parameter :: len = 2.d0*pi ! For fft 
  ! work arrays
  double precision, ALLOCATABLE, DIMENSION(:,:) :: u,f,x,y,DMrr,DMr,dm_coeff
  double precision, ALLOCATABLE, DIMENSION(:) :: theta,r,rh,rarray
  double complex, ALLOCATABLE, DIMENSION(:,:) :: fhat, uhat, what,&
       Amatrix,Bmatrix
  double complex, ALLOCATABLE, DIMENSION(:) :: w_c,dw_c,rhs
  integer, ALLOCATABLE, DIMENSION(:) :: ipiv
  integer :: i,j,info 

  call setupfft(ntheta,len)
  ALLOCATE(u(nrh,ntheta),f(nrh,ntheta),x(nrh,ntheta),y(nrh,ntheta))
  ALLOCATE(r(nr),theta(ntheta),rh(nrh))
  ALLOCATE(dmrr(nr,nr),dmr(nr,nr),dm_coeff(nr,0:2),rarray(nr))
  allocate(fhat(nrh,nthetah+1),uhat(nrh,nthetah+1),what(nrh,nthetah+1),&
       w_c(nr),dw_c(nr))
  allocate(amatrix(1:nr-2,1:nr-2),bmatrix(1:nr-2,1:nr-2),rhs(nr-2),ipiv(nr-2))

  ! Setup grids
  DO i=1,nr
   r(i)        = -cos(pi*dble(i-1)/dble(nr-1))
  END DO
  ! Positive part of r  
  DO i=1,nrh
   rh(nrh-i+1) = cos(pi*dble(i-1)/dble(nr-1))
  END DO
  ! Angular variable
  DO i=1,ntheta
   theta(i) = len*dble(i-1)/dble(ntheta)
  END DO
  ! For convenience we keep Cartesian coordinates as well
  do j = 1,ntheta
   do i = 1,nrh
    x(i,j) = rh(i)*cos(theta(j))
    y(i,j) = rh(i)*sin(theta(j))
   end do
  end do
  ! Compute differentiation matrices on the Chebyshev grid
  do i = 1,nr
   call weights1(r(i),r,nr-1,nr-1,2,dm_coeff)
   dmr(i,:)  = dm_coeff(:,1)
   dmrr(i,:) = dm_coeff(:,2)
  end do

  ! Below we will compute 
  ! f_{rr} + (1/r) * f_r + (1/r^2) * f_{\theta \theta} = rhs
  ! And then solve 
  ! u_{rr} + (1/r) * u_r + (1/r^2) * u_{\theta \theta} = rhs
  ! with zero Dirichlet bc.
  ! After Fourier transform the second equation becomes 
  ! v(k)_{rr} + (1/r) * v(k)_r - (k^2/r^2) * v(k) = FFT[rhs](k)
  ! In matrix form we have
  ! DMRR + diag(1/r) * DMR -k^2*diag(1/r^2)
  ! We store the k independent part in Amatrix
  amatrix = dmrr(2:nr-1,2:nr-1) 
  do i = 1,nr-2
   amatrix(i,:) = amatrix(i,:) + dmr(i+1,2:nr-1)/r(i+1)   
  end do
  
  ! The final solution. Should be zero on |r|=1.
  f = exp(-36.d0*((x-0.06d0)**2+(y-0.01d0)**2))*sin(35.d0*x)*sin(29.d0*y)
  
  ! Now compute
  ! f_{xx} + f_{yy} = f_{rr} + (1/r) * f_r + (1/r^2) * f_{\theta \theta}

  ! Go to wave number space 
  do i = 1,nrh
   u_r = f(i,:)
   call dfftw_execute (forward)
   uhat(i,:) = u_c
  end do
  do j = 1,nthetah+1
   do i = 1,nrh
    w_c(i)     = uhat(nrh+1-i,j)
    w_c(nrh+i) = uhat(i,j)
   end do
   ! Differentiate in r
   dw_c = matmul(dmrr,w_c) + matmul(dmr,w_c)/r
   do i = 1,nrh
    what(nrh+1-i,j) = dw_c(i)
    what(i,j)       = dw_c(nrh+i) 
   end do
  end do
  ! Differentiate in \theta
  do i = 1,nrh
   do j = 1,nthetah+1
    u_c(j) = (-1.0D0,0.0D0)*k(j)*k(j)*uhat(i,j)/rh(i)**2
   end do
   what(i,:) = what(i,:) + u_c
  end do
  ! Convert back, f now contains u_{xx}+u_{yy}
  do i = 1,nrh
   u_c = what(i,:)
   call dfftw_execute (backward)
   f(i,:) = u_r/dble(ntheta)
  end do
  call printdble2d(f,1,nrh,1,ntheta,'ulap.txt')
  call printdble2d(x,1,nrh,1,ntheta,'x.txt')
  call printdble2d(y,1,nrh,1,ntheta,'y.txt')

  ! Now solve u_{xx}+u_{yy} = f_{xx}+f_{yy}
  ! Go to wave number space 
  do i = 1,nrh
   u_r = f(i,:)
   call dfftw_execute (forward)
   fhat(i,:) = u_c
  end do
  ! Now invert for one mode at a time
  what = 0.d0
  do j = 1,nthetah+1
   bmatrix = amatrix
   do i = 1,nr-2
    bmatrix(i,i) = bmatrix(i,i) - k(j)**2/r(i+1)**2 
   end do
   ! Factor 
   call ZGETRF(nr-2,nr-2,bmatrix,nr-2,IPIV,INFO)
   do i = 1,nrh
    w_c(i)     = fhat(nrh+1-i,j)
    w_c(nrh+i) = fhat(i,j)
   end do
   rhs(1:nr-2) = w_c(2:nr-1)
   ! NOTE!!! RHS MUST BE UPDATED FOR NON-ZERO BC!
   ! First and last column of Amatrix * boundary condition
   ! should go into rhs.
   ! Solve
   call ZGETRS('n',nr-2,1,bmatrix,nr-2,IPIV,rhs,nr-2,INFO)
   ! W_C SHOULD BE UPDATED FOR NON-ZERO BC
   w_c = 0.d0
   w_c(2:nr-1) = rhs(1:nr-2) 
   do i = 1,nrh
    what(nrh+1-i,j) = w_c(i)
    what(i,j)       = w_c(nrh+i) 
   end do
  end do
  ! Convert back, f contains u solving u_{xx}+u_{yy} = f, and Dirichlet b.c.
  do i = 1,nrh
   u_c = what(i,:)
   call dfftw_execute (backward)
   u(i,:) = u_r/dble(ntheta)
  end do
  call printdble2d(u,1,nrh,1,ntheta,'u.txt')
  call printdble2d(x,1,nrh,1,ntheta,'x.txt')
  call printdble2d(y,1,nrh,1,ntheta,'y.txt')
  ! 
  DEALLOCATE(u,f,x,y)
  DEALLOCATE(r,theta,rh)
  DEALLOCATE(dmrr,dmr,dm_coeff,rarray)
  deallocate(fhat,uhat,what,w_c,dw_c)
  deallocate(amatrix,bmatrix,rhs,ipiv)
  ! 
  call destroyfft
  !   
end program MAIN

subroutine printdble2d(u,nx1,nx2,ny1,ny2,str)
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, intent(in) :: nx1,nx2,ny1,ny2
  real(dp), intent(in) :: u(nx1:nx2,ny1:ny2)
  character(len=*), intent(in) :: str
  integer :: i,j
  open(2,file=trim(str),status='unknown')
  do j=ny1,ny2,1
   do i=nx1,nx2,1
    if(abs(u(i,j)) .lt. 1e-40) then
     write(2,fmt='(E24.16)',advance='no') 0.d0
    else
     write(2,fmt='(E24.16)',advance='no') u(i,j)
    end if
   end do
   write(2,'()')
  end do
  close(2)
end subroutine printdble2d
