d = load('depol1.txt');
I1 = find(d(:,1) == 1);
I2 = find(d(:,1) == 2);
I3 = find(d(:,1) == 3);

%d(:,3) = d(:,3) * 100 /((2*pi)^2);

plot(d(I1,2), d(I1,3),'-k', ...
     d(I2,2), d(I2,3),'-r', ...
     d(I3,2), d(I3,3),'-b', 'linewidth',2)

