Welcome to Numerical experiments for reduced Bloch equations repository!

The folders so far include:

polar_laplace: the example of Fourier-Chebishev solver of the Poisson's equation

1dof: 1 degree of freedom RBE solver with spectral convergence tests.
It uses naive LU/Gaussian elimination lapack routines

polar_fp: 2 degree of freedom solver with spectral convergence tests.
It uses sparse LU via superlu routines for efficiency.

All codes are in Fortran 90 and can be complied by editing makefiles to point out the location of lapack/superlu and fftw libraries and running $make 

For more information feel free do email Oleksii Beznosov obeznosov@unm.edu .
