nth = 5;
nrh = 32;
nr = 32*2;
nt = nth*2;
len = 2*pi;
s= 8;
ss = s^2;
rmax = 8*s;

is = nrh-(1:nrh)+1;
ise = is(2:end);
rh(is) = rmax*cos(pi*(1:nrh)/(nr-1));

w = zeros(nrh-1,1);
w(ise) = rh(ise)*pi.*sin(pi*(1:nrh-1)/(nr-1)).^2 ./ ((nr-1) * sqrt(1-(r(ise)/rmax).^2));

w = w*rmax*2*pi/(nt-1);

f = zeros(nrh,nt);
for i = 1:nt
  f(:,i) = exp(-r.^2/(2*ss)) / (2*pi*ss);
end

int = 0.d0;
for i = 1:nt
  int = int + f(1:nrh-1,i)'*w;
end


disp(int)

