clear

load x.txt
load y.txt
load ulap.txt
load u.txt

X    = [x; x(1,:)];
Y    = [y; y(1,:)];
U    = [u; u(1,:)];
ULAP = [ulap; ulap(1,:)];
figure(1)
clf
contour(X,Y,ULAP,20)
hold on
contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
colorbar
set(gca,'fontsize',18)
axis equal
figure(2)
clf
contour(X,Y,U,20)
hold on
contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
colorbar
set(gca,'fontsize',18)
axis equal