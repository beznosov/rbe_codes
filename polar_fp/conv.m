B1=load('err01.dat');
B2=load('err02.dat');
B3=load('err03.dat');
semilogy(B1(:,1),B1(:,2),'-dk',...
B2(:,1),B2(:,2),'-*r',...
B3(:,1),B3(:,2),'-ob','linewidth',2)
ylabel('$|\varepsilon|_{\infty}$','Interpreter','latex')
xlabel('$n_{r_1},n_{r_2}$','Interpreter','latex')
grid on
axis([14,50,1e-8,1e-1])
lgn = legend('$\Delta\theta=1e-1$','$\Delta\theta=1e-2$','$\Delta\theta=1e-3$')
set(lgn,'Interpreter','latex')
set(gca,'fontsize',14)

