program main
  USE FFT
  !
  IMPLICIT NONE
  INTEGER, PARAMETER :: nthetah = 21 ! Half of the discretization points 
  INTEGER, PARAMETER :: nrh     = 20 ! Half of the discretization points 
  INTEGER, PARAMETER :: ntheta = 2*nthetah
  INTEGER, PARAMETER :: nr     = 2*nrh
  INTEGER, PARAMETER :: dims   = 3
  double precision,  parameter :: pi = 4.D0*DATAN(1.D0)
  double precision,  parameter :: len = 2.d0*pi ! For fft 
  double precision,  parameter :: tfinal = 2.d0*pi 
  double precision, parameter :: sig = 8.d0
  double precision, parameter :: sigs = sig**2 
  double precision, parameter :: rmax = 8.d0*sig
  double precision, parameter :: rmaxi = 0.125d0/sig
  ! work arrays
  double precision, ALLOCATABLE, DIMENSION(:,:) :: x,y,DMrr,DMr,dm_coeff
  double precision, ALLOCATABLE, DIMENSION(:,:,:) :: u, f, om
  double precision, ALLOCATABLE, DIMENSION(:) :: theta,r,rh,rarray
  double complex, ALLOCATABLE, DIMENSION(:,:) :: what, Amatrix, Bmatrix
  double complex, ALLOCATABLE, DIMENSION(:,:,:) :: fhat, uhat
  double complex, ALLOCATABLE, DIMENSION(:) :: w_c,dw_c,rhs
  integer, ALLOCATABLE, DIMENSION(:) :: ipiv
  integer :: i,j, dm, ts, ntsteps, info 
  double precision :: dt
  double precision, dimension(dims) :: P_A, P_B, P_M
  character(24) :: filename

  call setupfft(ntheta,len)
  ALLOCATE(u(nrh,ntheta,dims),f(nrh,ntheta,dims),om(nrh,ntheta,dims))
  allocate(x(nrh,ntheta),y(nrh,ntheta))
  ALLOCATE(r(nr),theta(ntheta),rh(nrh))
  ALLOCATE(dmrr(nr,nr),dmr(nr,nr),dm_coeff(nr,0:2),rarray(nr))
  allocate(fhat(nrh,nthetah+1,dims),uhat(nrh,nthetah+1,dims))
  allocate(what(nrh,nthetah+1), w_c(nr),dw_c(nr))
  allocate(amatrix(1:nr-2,1:nr-2),bmatrix(1:nr-2,1:nr-2),rhs(nr-2),ipiv(nr-2))

  P_A = 1.d0
  P_B = 1.d0
  P_M = sigs * P_A

  ! Setup grids
  DO i=1,nr
   r(i)        = -rmax*cos(pi*dble(i-1)/dble(nr-1))
  END DO
  ! Positive part of r  
  DO i=1,nrh
   rh(nrh-i+1) = rmax*cos(pi*dble(i-1)/dble(nr-1))
  END DO
  ! Angular variable
  DO i=1,ntheta
   theta(i) = len*dble(i-1)/dble(ntheta)
  END DO
  ! For convenience we keep Cartesian coordinates as well
  do j = 1,ntheta
   do i = 1,nrh
    x(i,j) = rh(i)*cos(theta(j))
    y(i,j) = rh(i)*sin(theta(j))
    om(i,j,1) = rh(i)*cos(theta(j))
    om(i,j,2) = rh(i)*sin(theta(j))
    om(i,j,3) = 1.d0
   end do
  end do
  ! Compute differentiation matrices on the Chebyshev grid
  do i = 1,nr
   call weights1(r(i),r,nr-1,nr-1,2,dm_coeff)
   dmr(i,:)  = dm_coeff(:,1)
   dmrr(i,:) = dm_coeff(:,2)
  end do
  dt = 0.1*(r(1) - r(2))**2
  write(*,*) 'dt = ', dt

  ! Below we will compute 
  ! f_{rr} + (1/r) * f_r + (1/r^2) * f_{\theta \theta} = rhs
  ! And then solve 
  ! u_{rr} + (1/r) * u_r + (1/r^2) * u_{\theta \theta} = rhs
  ! with zero Dirichlet bc.
  ! After Fourier transform the second equation becomes 
  ! v(k)_{rr} + (1/r) * v(k)_r - (k^2/r^2) * v(k) = FFT[rhs](k)
  ! In matrix form we have
  ! DMRR + diag(1/r) * DMR -k^2*diag(1/r^2)
  ! We store the k independent part in Amatrix
  amatrix = sigs * dmrr(2:nr-1,2:nr-1) 
  do i = 1,nr-2
   amatrix(i,:) = amatrix(i,:) + sigs*dmr(i+1,2:nr-1)/r(i+1) + dmr(i+1,2:nr-1)*r(i+1)
   amatrix(i,i) = amatrix(i,i) + 2.d0
  end do

  ! Omega vector

  ! The initial coundition, we set 
  u(:,:,:) = 0.d0
!  u(:,:,1) = exp(-(x**2 + y**2) / (2.d0*sigs))/(2*pi*sigs) &
!                             * (1.d0 + x**2) / (1.d0 + sigs)
!  u(:,:,2) = exp(-(x**2 + y**2) / (2.d0*sigs))/(2*pi*sigs) &
!                             * (1.d0 + x**2) / (1.d0 + sigs)
  u(:,:,3) = exp(-(x**2 + y**2) / (2.d0*sigs))/(2*pi*sigs) &
                             * (1.d0 + x**2) / (1.d0 + sigs)
  write(filename,'("u",I1.1,"_",I8.8,".txt")') 1, 0
  call printdble2d(u(:,:,1),1,nrh,1,ntheta,trim(filename))

  ! Compute dt and number of timesteps
  ntsteps = ceiling(tfinal / dt)
  dt = tfinal / ntsteps
  
  ! Now start the timestepping
  do ts = 1, ntsteps
  write(*,*) 'Timestep ', ts, '/', ntsteps

  ! Now solve (I - dtL_{FP})u^{nu+1}  = f = u^{nu} + dt * om \cross u
  ! Go to wave number space 
  do dm = 1,dims
   do i = 1,nrh
    if (dm == 1) then
     u_r = u(i,:,dm) + dt*(cos(dt*(ts-1))a*u(i,:,3) - u(i,:,2));
    elseif (dm == 2) then
     u_r = u(i,:,dm) + dt*(u(i,:,1) - sin(dt*(ts-1))*u(i,:,3));
    else
     u_r = u(i,:,dm) + dt*(cos(dt*(ts-1))*u(i,:,2) - sin(dt*(ts-1))*u(i,:,1));
    endif
    call dfftw_execute (forward)
    fhat(i,:,dm) = u_c
   end do
  enddo

  do dm = 1,dims
   ! Now invert for one mode at a time
   what = 0.d0
   do j = 1,nthetah+1
    bmatrix = dt * P_A(dm) * amatrix
    do i = 1,nr-2
      bmatrix(i,i) = -1.d0 +   bmatrix(i,i) - dt*P_B(dm)*complex(0.d0, 1.d0)*k(j) &
                                           - dt*sigs*P_A(dm)*k(j)**2/r(i+1)**2 
    end do

    bmatrix = -bmatrix
    ! Factor 
    call ZGETRF(nr-2,nr-2,bmatrix,nr-2,IPIV,INFO)
    do i = 1,nrh
     w_c(i)     = fhat(nrh+1-i,j,dm)
     w_c(nrh+i) = fhat(i,j,dm)
    end do
    rhs(1:nr-2) = w_c(2:nr-1)
    ! NOTE!!! RHS MUST BE UPDATED FOR NON-ZERO BC!
    ! First and last column of Amatrix * boundary condition
    ! should go into rhs.
    ! Solve
    call ZGETRS('n',nr-2,1,bmatrix,nr-2,IPIV,rhs,nr-2,INFO)
    ! W_C SHOULD BE UPDATED FOR NON-ZERO BC
    w_c = 0.d0
    w_c(2:nr-1) = rhs(1:nr-2) 
    do i = 1,nrh
     what(nrh+1-i,j) = w_c(i)
     what(i,j)       = w_c(nrh+i) 
    end do
   end do
   ! Convert back, f contains u solving u_{xx}+u_{yy} = f, and Dirichlet b.c.
   do i = 1,nrh
    u_c = what(i,:)
    call dfftw_execute (backward)
    u(i,:,dm) = u_r/dble(ntheta)
   end do
   write(filename,'("u",I1.1,"_",I8.8,".txt")') dm, ts
   call printdble2d(u(:,:,dm),1,nrh,1,ntheta,trim(filename))
  enddo
  enddo
  call printdble2d(x,1,nrh,1,ntheta,'x.txt')
  call printdble2d(y,1,nrh,1,ntheta,'y.txt')
  ! 
  DEALLOCATE(u,f,x,y)
  DEALLOCATE(r,theta,rh)
  DEALLOCATE(dmrr,dmr,dm_coeff,rarray)
  deallocate(fhat,uhat,what,w_c,dw_c)
  deallocate(amatrix,bmatrix,rhs,ipiv)
  ! 
  call destroyfft
  !   
end program MAIN

subroutine printdble2d(u,nx1,nx2,ny1,ny2,str)
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, intent(in) :: nx1,nx2,ny1,ny2
  real(dp), intent(in) :: u(nx1:nx2,ny1:ny2)
  character(len=*), intent(in) :: str
  integer :: i,j
  open(2,file=trim(str),status='unknown')
  do j=ny1,ny2,1
   do i=nx1,nx2,1
    if(abs(u(i,j)) .lt. 1e-40) then
     write(2,fmt='(E24.16)',advance='no') 0.d0
    else
     write(2,fmt='(E24.16)',advance='no') u(i,j)
    end if
   end do
   write(2,'()')
  end do
  close(2)
end subroutine printdble2d
