program main
  USE FFT
  !
  IMPLICIT NONE
  INTEGER, PARAMETER :: nthetah = 5 ! Half of the discretization points 
  INTEGER :: nrh   !  = 20 ! Half of the discretization points 
  INTEGER, PARAMETER :: ntheta = 2*nthetah
  INTEGER :: nr   !  = 2*nrh
  INTEGER, PARAMETER :: dims   = 3
  double precision,  parameter :: pi = 4.D0*DATAN(1.D0)
  double precision,  parameter :: len = 2.d0*pi ! For fft 
  double precision,  parameter :: tfinal = 2.d0*pi
  double precision, parameter :: sig = 1.d0/sqrt(2.d0)
  double precision, parameter :: sigs = sig**2 
  double precision, parameter :: rmax = 8.d0
  double precision, parameter :: rmaxi = 0.125d0
  ! work arrays
  double precision, ALLOCATABLE, DIMENSION(:,:) :: x,y,DMrr,DMr,dm_coeff
  double precision, ALLOCATABLE, DIMENSION(:,:,:) :: u, f, om
  double precision, ALLOCATABLE, DIMENSION(:,:) :: uout
  double precision, ALLOCATABLE, DIMENSION(:,:,:) :: uex
  double precision, ALLOCATABLE, DIMENSION(:,:,:) :: up
  double precision, ALLOCATABLE, DIMENSION(:) :: theta,r,rh,rarray,w
  double complex, ALLOCATABLE, DIMENSION(:,:) :: what, Amatrix, Bmatrix
  double complex, ALLOCATABLE, DIMENSION(:,:,:) :: fhat, uhat, ohat  
  ! Storage for previous step
  double complex, ALLOCATABLE, DIMENSION(:,:,:) :: fhatp, uhatp, ohatp
  double complex, ALLOCATABLE, DIMENSION(:) :: w_c,dw_c,rhs
  integer, ALLOCATABLE, DIMENSION(:) :: ipiv
  integer :: i,j, dm, ts, ntsteps, info 
  double precision :: dt, integ
  double precision, dimension(dims) :: P_A, P_B, P_M
  character(24) :: filename


  do nrh = 32,32,2
  nr = 2*nrh

  call setupfft(ntheta,len)
  ALLOCATE(u(nrh,ntheta,dims),f(nrh,ntheta,dims),om(nrh,ntheta,dims))
  ALLOCATE(up(nrh,ntheta,dims))
  ALLOCATE(uex(nrh,ntheta,dims))
  ALLOCATE(uout(nrh,ntheta))
  allocate(x(nrh,ntheta),y(nrh,ntheta))
  ALLOCATE(r(nr),theta(ntheta),rh(nrh),w(nrh-1))
  ALLOCATE(dmrr(nr,nr),dmr(nr,nr),dm_coeff(nr,0:2),rarray(nr))
  allocate(fhat(nrh,nthetah+1,dims),uhat(nrh,nthetah+1,dims))
  allocate(fhatp(nrh,nthetah+1,dims),uhatp(nrh,nthetah+1,dims))
  allocate(ohatp(nrh,nthetah+1,dims), ohat(nrh, nthetah+1,dims))
  allocate(what(nrh,nthetah+1), w_c(nr),dw_c(nr))
  allocate(amatrix(1:nr-2,1:nr-2),bmatrix(1:nr-2,1:nr-2),rhs(nr-2),ipiv(nr-2))

  P_A = 1.d0
  P_B = 1.d0
  P_M = sigs * P_A

  ! Setup grids
  DO i=1,nr
   r(i)        = -rmax*cos(pi*dble(i-1)/dble(nr-1))
  END DO
  ! Positive part of r  
  DO i=1,nrh
   rh(nrh-i+1) = rmax*cos(pi*dble(i-1)/dble(nr-1))
  END DO

  ! Angular variable
  DO i=1,ntheta
   theta(i) = len*dble(i-1)/dble(ntheta)
  END DO
  ! For convenience we keep Cartesian coordinates as well
  do j = 1,ntheta
   do i = 1,nrh
    x(i,j) = rh(i)*cos(theta(j))
    y(i,j) = rh(i)*sin(theta(j))
 !   om(i,j,1) = 0.d0 ! rh(i)*cos(theta(j))
 !   om(i,j,2) = 0.d0 ! rh(i)*sin(theta(j))
 !   om(i,j,3) = 1.d0
   end do
  end do
  ! Compute differentiation matrices on the Chebyshev grid
  do i = 1,nr
   call weights1(r(i),r,nr-1,nr-1,2,dm_coeff)
   dmr(i,:)  = dm_coeff(:,1)
   dmrr(i,:) = dm_coeff(:,2)
  end do
  dt = 0.001 ! (r(2) - r(1))
 ! write(*,*) 'dt = ', dt

  ! Below we will compute 
  ! f_{rr} + (1/r) * f_r + (1/r^2) * f_{\theta \theta} = rhs
  ! And then solve 
  ! u_{rr} + (1/r) * u_r + (1/r^2) * u_{\theta \theta} = rhs
  ! with zero Dirichlet bc.
  ! After Fourier transform the second equation becomes 
  ! v(k)_{rr} + (1/r) * v(k)_r - (k^2/r^2) * v(k) = FFT[rhs](k)
  ! In matrix form we have
  ! DMRR + diag(1/r) * DMR -k^2*diag(1/r^2)
  ! We store the k independent part in Amatrix
  amatrix = sigs * dmrr(2:nr-1,2:nr-1) 
  do i = 1,nr-2
   amatrix(i,:) = amatrix(i,:) + sigs*dmr(i+1,2:nr-1)/r(i+1) + dmr(i+1,2:nr-1)*r(i+1)
   amatrix(i,i) = amatrix(i,i) + 2.d0
  end do

  ! Omega vector
  om(:,:,3) = 3.d0
  om(:,:,3) = 4.d0
  om(:,:,3) = 1.d0

  ! The initial coundition, we set 
  u(:,:,:) = 0.d0
  u(:,:,1) = exp(-(x**2 + y**2) / (2.d0*sigs))/(2*pi*sigs) &
                            * (2.d0 + x**2) / (1.d0 + sigs)
  u(:,:,2) = 0.d0*exp(-(x**2 + y**2) / (2.d0*sigs))/(2*pi*sigs) &
                            * (2.d0 + x**2) / (1.d0 + sigs)
  u(:,:,3) = 0.d0*exp(-(x**2 + y**2) / (2.d0*sigs))/(2*pi*sigs) &
                            * (2.d0 + x**2) / (1.d0 + sigs)

  uex(:,:,1) = (cos(tfinal) - sin(tfinal))*u(:,:,3)
  uex(:,:,2) = (sin(tfinal) + cos(tfinal))*u(:,:,3)
  uex(:,:,3) = u(:,:,3)

  do dm = 1,dims
   do i = 1,ntheta
    uout(:,i) = u(:,i,dm)*rh
   enddo
   write(filename,'("u",I1.1,"_",I8.8,".txt")') dm, 0
   call printdble2d(uout,1,nrh,1,ntheta,trim(filename))
  enddo
  ! Compute dt and number of timesteps
  ntsteps = ceiling(tfinal / dt)
  dt = tfinal / ntsteps
  
  ! Now start the timestepping
  ! First timestep to preheat BDF scheme 
  ! Second order explicit RK
  ! k1 = hf(u0)
  ! k2 = hf(u0 + 1/2k1)
  ! u1 = u0 + k2,
  ! Go to wave number space 
  do dm = 1,dims
   do i = 1,nrh
   ! Transform the solution
    u_r = u(i,:,dm)
    call dfftw_execute (forward)
    uhat(i,:,dm) = u_c 
   ! Transform the coupling term
    call getCouplingTerm(u(i,:,:),om(i,:,:),u_r,1,ntheta,dm)
    call dfftw_execute (forward)
    ohatp(i,:,dm) = u_c 
    fhat(i,:,dm) = u_c 
   end do
  enddo
  uhatp(:,:,:) = uhat(:,:,:)

  do dm = 1,dims
   ! Now invert for one mode at a time
   what = 0.d0
   do j = 1,nthetah+1
    bmatrix = dt * P_A(dm) * amatrix
    do i = 1,nr-2
      bmatrix(i,i) =  bmatrix(i,i) - & 
                                 dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j) &
                                    +sigs*P_A(dm)*k(j)**2/r(i+1)**2) 
    end do

    ! Factor 
    do i = 1,nrh
     w_c(i)     = uhat(nrh+1-i,j,dm)
     w_c(nrh+i) = uhat(i,j,dm)
    end do
    rhs(1:nr-2) = MATMUL(bmatrix, w_c(2:nr-1))
    do i = 1,nrh
     w_c(i)     = fhat(nrh+1-i,j,dm)
     w_c(nrh+i) = fhat(i,j,dm)
    end do
    rhs(1:nr-2) = rhs(1:nr-2) + dt*w_c(2:nr-1)
    ! NOTE!!! RHS MUST BE UPDATED FOR NON-ZERO BC!
    ! First and last column of Amatrix * boundary condition
    ! should go into rhs.
    ! Solve
    ! W_C SHOULD BE UPDATED FOR NON-ZERO BC
    w_c = 0.d0
    w_c(2:nr-1) = rhs(1:nr-2) 
    do i = 1,nrh
     what(nrh+1-i,j) = w_c(i)
     what(i,j)       = w_c(nrh+i) 
    end do
   end do
   ! Convert back, f contains u solving u_{xx}+u_{yy} = f, and Dirichlet b.c.
   do i = 1,nrh
    u_c = what(i,:)
    call dfftw_execute (backward)
    up(i,:,dm) = u_r/dble(ntheta)
   end do
  enddo


  do dm = 1,dims
   do i = 1,nrh
    u_r = u(i,:,dm)
    call dfftw_execute (forward)
    uhat(i,:,dm) = u_c

    call getCouplingTerm(u(i,:,:)+0.5d0*up(i,:,:),om(i,:,:),u_r,1,ntheta,dm)
    call dfftw_execute (forward)
    fhat(i,:,dm) = u_c 
   end do
  enddo

  do dm = 1,dims
   ! Now invert for one mode at a time
   what = 0.d0
   do j = 1,nthetah+1
    bmatrix = dt * P_A(dm) * amatrix
    do i = 1,nr-2
      bmatrix(i,i) =  bmatrix(i,i) - & 
                                 dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j) &
                                    +sigs*P_A(dm)*k(j)**2/r(i+1)**2) 
    end do

    ! Factor 
    do i = 1,nrh
     w_c(i)     = uhat(nrh+1-i,j,dm)
     w_c(nrh+i) = uhat(i,j,dm)
    end do
    rhs(1:nr-2) = MATMUL(bmatrix, w_c(2:nr-1))
    do i = 1,nrh
     w_c(i)     = fhat(nrh+1-i,j,dm)
     w_c(nrh+i) = fhat(i,j,dm)
    end do
    rhs(1:nr-2) = rhs(1:nr-2) + dt*w_c(2:nr-1)
    ! NOTE!!! RHS MUST BE UPDATED FOR NON-ZERO BC!
    ! First and last column of Amatrix * boundary condition
    ! should go into rhs.
    ! Solve
    ! W_C SHOULD BE UPDATED FOR NON-ZERO BC
    w_c = 0.d0
    w_c(2:nr-1) = rhs(1:nr-2) 
    do i = 1,nrh
     what(nrh+1-i,j) = w_c(i)
     what(i,j)       = w_c(nrh+i) 
    end do
   end do
   ! Convert back, f contains u solving u_{xx}+u_{yy} = f, and Dirichlet b.c.
   do i = 1,nrh
    u_c = what(i,:)
    call dfftw_execute (backward)
    u(i,:,dm) = u(i,:,dm) + u_r/dble(ntheta)
   end do 
   do i = 1,ntheta
    uout(:,i) = u(:,i,dm)*rh
   enddo
   write(filename,'("u",I1.1,"_",I8.8,".txt")') dm, 1
   call printdble2d(uout,1,nrh,1,ntheta,trim(filename))
  enddo
  ! Proceed with multistep
  do ts = 2, ntsteps
!  write(*,*) 'Timestep ', ts, '/', ntsteps

  ! Now solve 
  !(I - 2/3dtL_{FP})u^{nu+1}  = 4/3u^{nu} - 1/3u^{nu} 
  !                           + 4/3dt * (omega \cross u^{nu} - omega \cross u^{nu-1})
  ! Go to wave number space 
  do dm = 1,dims
   do i = 1,nrh
    u_r = u(i,:,dm)
    call dfftw_execute (forward)
    uhat(i,:,dm) = u_c 

    call getCouplingTerm(u(i,:,:),om(i,:,:),u_r,1,ntheta,dm)
    call dfftw_execute (forward)
    ohat(i,:,dm) = u_c
   end do
  enddo
  fhat =(2.d0*uhat - 0.5d0 * uhatp + 2.d0*dt*ohat - dt*ohatp)
  ohatp = ohat
  uhatp = uhat

  do dm = 1,dims
   ! Now invert for one mode at a time
   what = 0.d0
   do j = 1,nthetah+1
    bmatrix = dt *  P_A(dm) * amatrix
    do i = 1,nr-2
      bmatrix(i,i) = -1.5d0 + bmatrix(i,i) &
                        - dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j) &
                                    +sigs*P_A(dm)*k(j)**2/r(i+1)**2) 
    end do

    bmatrix = -bmatrix
    ! Factor 
    call ZGETRF(nr-2,nr-2,bmatrix,nr-2,IPIV,INFO)
    do i = 1,nrh
     w_c(i)     = fhat(nrh+1-i,j,dm)
     w_c(nrh+i) = fhat(i,j,dm)
    end do
    rhs(1:nr-2) = w_c(2:nr-1)
    ! NOTE!!! RHS MUST BE UPDATED FOR NON-ZERO BC!
    ! First and last column of Amatrix * boundary condition
    ! should go into rhs.
    ! Solve
    call ZGETRS('n',nr-2,1,bmatrix,nr-2,IPIV,rhs,nr-2,INFO)
    ! W_C SHOULD BE UPDATED FOR NON-ZERO BC
    w_c = 0.d0
    w_c(2:nr-1) = rhs(1:nr-2) 
    do i = 1,nrh
     what(nrh+1-i,j) = w_c(i)
     what(i,j)       = w_c(nrh+i) 
    end do
   end do
   ! Convert back, f contains u solving u_{xx}+u_{yy} = f, and Dirichlet b.c.
   do i = 1,nrh
    u_c = what(i,:)
    call dfftw_execute (backward)
    u(i,:,dm) = u_r/dble(ntheta)
   end do
   do i = 1,ntheta
    uout(:,i) = u(:,i,dm)*rh
   enddo
   write(filename,'("u",I1.1,"_",I8.8,".txt")') dm, ts
   call printdble2d(uout,1,nrh,1,ntheta,trim(filename))
   !call integrate(u(:,:,dm), w, nrh, ntheta, integ)
   !write(*,*) dm, dt*ts, integ 
  enddo
  enddo
  call printdble2d(x,1,nrh,1,ntheta,'x.txt')
  call printdble2d(y,1,nrh,1,ntheta,'y.txt')
  ! 


 ! write(*,*) nr, maxval(abs(u(:,:,:)-uex(:,:,:)))
  DEALLOCATE(u,f,x,y, uhatp, ohatp, ohat, om, up, uex, fhatp)
  DEALLOCATE(r,theta,rh)
  DEALLOCATE(dmrr,dmr,dm_coeff,rarray)
  deallocate(fhat,uhat,what,w_c,dw_c)
  deallocate(amatrix,bmatrix,rhs,ipiv)
  ! 
  call destroyfft

  enddo
  !   
end program MAIN

subroutine printdble2d(u,nx1,nx2,ny1,ny2,str)
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, intent(in) :: nx1,nx2,ny1,ny2
  real(dp), intent(in) :: u(nx1:nx2,ny1:ny2)
  character(len=*), intent(in) :: str
  integer :: i,j
  open(2,file=trim(str),status='unknown')
  do j=ny1,ny2,1
   do i=nx1,nx2,1
    if(abs(u(i,j)) .lt. 1e-40) then
     write(2,fmt='(E24.16)',advance='no') 0.d0
    else
     write(2,fmt='(E24.16)',advance='no') u(i,j)
    end if
   end do
   write(2,'()')
  end do
  close(2)
end subroutine printdble2d

subroutine getCouplingTerm(u,om,ct, nx1, nx2, dm)
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, intent(in) :: nx1,nx2,dm
  real(dp), intent(in) :: u(nx1:nx2,3)
  real(dp), intent(in) :: om(nx1:nx2,3)
  real(dp), intent(out) :: ct(nx1:nx2)
  integer :: i,j  

  if (dm == 1) then
     ct = (om(:,2)*u(:,3) - om(:,3)*u(:,2))
    elseif (dm == 2) then
     ct = (om(:,3)*u(:,1) - om(:,1)*u(:,3))
    else
     ct = (om(:,1)*u(:,2) - om(:,2)*u(:,1))
  endif
end subroutine getCouplingTerm

