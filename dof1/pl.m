clear

load x.txt
load y.txt
%load ulap.txt
ls1 = dir('u1*.txt')
ls2 = dir('u2*.txt')
ls3 = dir('u3*.txt')

X    = [x; x(1,:)];
Y    = [y; y(1,:)];

figure(2)
clf
for fl = 1:20:length(ls1)
subplot(1,3,1);
u1 = load(ls1(fl).name);
U    = [u1; u1(1,:)];
contour(X,Y,U, 30)
caxis([0 1e-1])
axis([-3 3 -3 3])
axis equal
hold on
contour(X,Y,X.^2+Y.^2)
colorbar
set(gca,'fontsize',18)
hold off
subplot(1,3,2);
title(['ts = ' num2str(fl)])
u1 = load(ls2(fl).name);
U    = [u1; u1(1,:)];
contour(X,Y,U, 30)
caxis([0 1e-1])
axis([-3 3 -3 3])
axis equal
hold on
contour(X,Y,X.^2+Y.^2)
colorbar
set(gca,'fontsize',18)
hold off
drawnow
subplot(1,3,3);
u1 = load(ls3(fl).name);
U    = [u1; u1(1,:)];
contour(X,Y,U, 30)
caxis([0 1e-1])
axis([-3 3 -3 3])
axis equal
hold on
contour(X,Y,X.^2+Y.^2)
colorbar
set(gca,'fontsize',14,'fontname','Times')
hold off
end
