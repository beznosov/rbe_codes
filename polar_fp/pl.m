clear

load x.txt
load y.txt
%load ulap.txt
ls1 = dir('e1*.txt')
ls2 = dir('e2*.txt')
ls3 = dir('e3*.txt')

X    = [x; x(1,:)];
Y    = [y; y(1,:)];

figure(2)
clf
for fl = 1:20:length(ls1)
title(['ts = ' num2str(fl)])
subplot(1,3,1);
u1 = load(ls1(fl).name);
U    = [u1; u1(1,:)];
contour(X,Y,U, 30)
caxis([0 1e-3])
hold on
contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
colorbar
set(gca,'fontsize',18)
axis equal
hold off
subplot(1,3,2);
u1 = load(ls2(fl).name);
U    = [u1; u1(1,:)];
contour(X,Y,U, 30)
caxis([0 1e-3])
hold on
contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
colorbar
set(gca,'fontsize',18)
axis equal
hold off
drawnow
subplot(1,3,3);
u1 = load(ls3(fl).name);
U    = [u1; u1(1,:)];
contour(X,Y,U, 30)
caxis([0 1e-3])
hold on
contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
colorbar
set(gca,'fontsize',18)
axis equal
hold off
end
