module FFT
  IMPLICIT NONE
  INCLUDE 'fftw3.f'                      ! needed for defining the plan
  INTEGER*8 :: forward,     backward     ! "plans" in FFTW
  ! work arrays
  double precision,    ALLOCATABLE, DIMENSION(:) :: u_r
  double complex,      ALLOCATABLE, DIMENSION(:) :: u_c
  ! wavenumber array
  double precision,    ALLOCATABLE, DIMENSION(:) :: k
contains
  SUBROUTINE setupfft(n,len)
    IMPLICIT NONE
    INCLUDE 'fftw3.f'           ! needed for defining the plan
    INTEGER i,n
    double precision pi,len
    pi = 4.D0*DATAN(1.D0)
    ! allocate work arrays
    ALLOCATE( u_r(n) )
    ALLOCATE( u_c(n/2+1) )
    ! allocate wavenumbers
    ALLOCATE( k(n/2+1))
    ! call initialization routine for FFTW (use -lfftw3 -lm for compilation)
    CALL dfftw_plan_dft_r2c_1d(forward ,n,u_r,u_c, FFTW_PATIENT)
    CALL dfftw_plan_dft_c2r_1d(backward,n,u_c,u_r, FFTW_PATIENT)
    ! compute wavenumbers
    DO i=1,(n/2+1)
     k(i) = DBLE(i-1)*2.d0*pi/len
    END DO
  END SUBROUTINE setupfft
  
  SUBROUTINE destroyfft
    CALL dfftw_destroy_plan(forward)
    CALL dfftw_destroy_plan(backward)
    DEALLOCATE(k,u_r,u_c)
  END SUBROUTINE destroyfft
  
  SUBROUTINE deriv(u,up,order,n)
    IMPLICIT NONE
    INTEGER :: n,order,i
    double precision, dimension(n) :: u,up
    u_r = u
    call dfftw_execute (forward)
    if (order .eq. 1) then
       DO i=1,(n/2+1)
          u_c(i) = (0.0D0,1.0D0)*k(i)*u_c(i)
       END DO
    END if
    if (order .eq. 2) then
       DO i=1,(n/2+1)
          u_c(i) = (-1.0D0,0.0D0)*k(i)*k(i)*u_c(i)
       END DO
    end if
    call dfftw_execute (backward)
    up = u_r/dble(n)
  end SUBROUTINE deriv

end module FFT
