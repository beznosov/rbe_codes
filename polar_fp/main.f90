program main
  USE FFT2
  !
  IMPLICIT NONE
  INTEGER, PARAMETER :: nthetah = 20 ! Half of the discretization points 
  INTEGER, PARAMETER :: maxnrh = 32 ! Maximum Half of the discretization points 
  INTEGER, PARAMETER :: maxnr = 2*maxnrh ! Max of the discretization points 
  INTEGER :: nrh1,nrh2   !  = 20 ! Half of the discretization points 
  INTEGER, PARAMETER :: ntheta = 2*nthetah
  INTEGER :: nr1,nr2   !  = 2*nrh
  INTEGER, PARAMETER :: dims   = 3
  double precision,  parameter :: pi = 4.D0*DATAN(1.D0)
  double precision,  parameter :: len = 2.d0*pi ! For fft 
  double precision,  parameter :: tfinal = 0.01d0*pi/4.d0
  double precision, parameter :: sig = 1.d0/sqrt(2.d0)
  double precision, parameter :: sigs = sig**2 
  double precision, parameter :: rmax = 8.d0*sig
  double precision, parameter :: rmaxi = 0.125d0/sig
  ! SuperLU stuff
  integer, parameter :: maxn = (maxnr-2)*(maxnr-2)
  integer, parameter :: maxnz = 2*(maxnr-2)**3 - (maxnr-2)**2
  integer :: rowind(maxnz), colptr(maxn+1)
  double complex, allocatable, dimension (:) ::b
  double complex, allocatable, dimension (:,:,:) :: values
  integer :: n, nnz

  ! work arrays
  double precision, ALLOCATABLE, DIMENSION(:,:) :: x1,y1,DMrr1,DMr1,dm_coeff1
  double precision, ALLOCATABLE, DIMENSION(:,:) :: x2,y2,DMrr2,DMr2,dm_coeff2
  double precision, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: u, up, f, om ! All we need in physical space       
  double precision, ALLOCATABLE, DIMENSION(:,:,:,:) :: uout         ! For output
  double precision, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: uex        ! Exact solution for convergence
  double precision, ALLOCATABLE, DIMENSION(:) :: theta,r1,rh1,r2,rh2,xcc,wcc        ! Grids
  double complex, ALLOCATABLE, DIMENSION(:,:) ::  Amatrix1, Amatrix2, Bmatrix1, Bmatrix2 ! 
  double complex, ALLOCATABLE, DIMENSION(:,:,:,:) :: what  ! 
  double complex, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: fhat, uhat, ohat ! in fourier space
  ! Storage for previous step
  double complex, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: fhatp, uhatp, ohatp
  double complex, ALLOCATABLE, DIMENSION(:,:) :: w_c,rhs
  integer, ALLOCATABLE, DIMENSION(:) :: ipiv
  integer :: i,start,i1,j1,i2,j2, dm, ts, ntsteps, info,ldb
  integer*8 :: factors(nthetah+1,nthetah+1)
  double precision :: dt, integ
  double precision, dimension(dims) :: P_A, P_B, P_M
  character(24) :: filename

  allocate(values(maxnz,nthetah+1, nthetah+1), b(maxn))

  open(2,file=trim('err.txt'),status='unknown')

  dt = 0.001 ! (r(2) - r(1))

  do nrh1 = 24,24,2
  nr1 = 2*nrh1
  nrh2 = nrh1
  nr2 = 2*nrh2
  n = (nr1-2)*(nr2-2)
  nnz = (nr1-2)**2*(nr2-2)+(nr2-2)**2*(nr1-2) - (nr2-2)*(nr1-2)
  ldb = n
!  nnz = n+1
!  write(*,*) (n+1)**2, nnz
!  ldb = n
!
!  do i = 1, n+1
!   values(i) = 1.d0
!   b(i) = i
!   rowind(i) = i
!   colptr(i) = i
!  enddo
!  call c_fortran_zgssv(1,n,nnz,1,values,rowind,colptr,b,ldb,factors,info)
!  write(*,*) 1
!  call c_fortran_zgssv(2,n,nnz,1,values,rowind,colptr,b,ldb,factors,info)
!  call c_fortran_zgssv(3,n,nnz,1,values,rowind,colptr,b,ldb,factors,info)
!
!  write(*,*) b(1:n+1)
!  stop

  call setupfft(ntheta,len)
  allocate(amatrix1(1:nr1-2,1:nr1-2), amatrix2(1:nr2-2,1:nr2-2), &
           bmatrix1(1:nr1-2,1:nr1-2), bmatrix2(1:nr2-2,1:nr2-2),rhs(nr1-2,nr2-2))
  ALLOCATE(u(nrh1,nrh2,ntheta,ntheta,dims),f(nrh1,nrh2,ntheta,ntheta,dims),om(nrh1,nrh2,ntheta,ntheta,dims))
  ALLOCATE(up(nrh1,nrh2,ntheta,ntheta,dims))
  ALLOCATE(uex(nrh1,nrh2,ntheta,ntheta,dims))
  ALLOCATE(uout(nrh1,nrh2,ntheta,ntheta))
  allocate(x1(nrh1,ntheta),y1(nrh1,ntheta))
  allocate(x2(nrh2,ntheta),y2(nrh2,ntheta))
  ALLOCATE(r1(nr1),r2(nr2),theta(ntheta),rh1(nrh1), rh2(nrh2),wcc(nr2),xcc(nr2))
  ALLOCATE(dmrr1(nr1,nr1),dmr1(nr1,nr1),dm_coeff1(nr1,0:2))
  ALLOCATE(dmrr2(nr2,nr2),dmr2(nr2,nr2),dm_coeff2(nr2,0:2))
  allocate(fhat(nrh1,nrh2,nthetah+1,nthetah+1,dims),uhat(nrh1,nrh2,nthetah+1,nthetah+1,dims))
  allocate(fhatp(nrh1,nrh2,nthetah+1,nthetah+1,dims),uhatp(nrh1,nrh2,nthetah+1,nthetah+1,dims))
  allocate(ohatp(nrh1,nrh2,nthetah+1,nthetah+1,dims), ohat(nrh1,nrh2,nthetah+1, nthetah+1,dims))
  allocate(what(nrh1,nrh2,nthetah+1,nthetah+1), w_c(nr1,nr2))

  P_A = 1.d0
  P_B = 1.d0
  P_M = sigs * P_A

  ! Setup grids
  DO i1=1,nr1
   r1(i1)        = -rmax*cos(pi*dble(i1-1)/dble(nr1-1))
  END DO
  ! Positive part of r  
  DO i1=1,nrh1
   rh1(nrh1-i1+1) = rmax*cos(pi*dble(i1-1)/dble(nr1-1))
  END DO

  DO i2=1,nr2
   r2(i2)        = -rmax*cos(pi*dble(i2-1)/dble(nr2-1))
  END DO
  ! Positive part of r  
  DO i2=1,nrh2
   rh2(nrh2-i2+1) = rmax*cos(pi*dble(i2-1)/dble(nr2-1))
  END DO


  ! Angular variable
  DO i1=1,ntheta
   theta(i1) = len*dble(i1-1)/dble(ntheta)
  END DO
  ! For convenience we keep Cartesian coordinates as well
  do j1 = 1,ntheta
   do i1 = 1,nrh1
    x1(i1,j1) = rh1(i1)*cos(theta(j1))
    y1(i1,j1) = rh1(i1)*sin(theta(j1))
   end do
  end do
  do j2 = 1,ntheta
   do i2 = 1,nrh2
    x2(i2,j2) = rh2(i2)*cos(theta(j2))
    y2(i2,j2) = rh2(i2)*sin(theta(j2))
   end do
  end do
  ! Compute differentiation matrices on the Chebyshev grid
  do i1 = 1,nr1
   call weights1(r1(i1),r1,nr1-1,nr1-1,2,dm_coeff1)
   dmr1(i1,:)  = dm_coeff1(:,1)
   dmrr1(i1,:) = dm_coeff1(:,2)
  end do
  do i2 = 1,nr2
   call weights1(r2(i2),r2,nr2-1,nr2-1,2,dm_coeff2)
   dmr2(i2,:)  = dm_coeff2(:,1)
   dmrr2(i2,:) = dm_coeff2(:,2)
  end do
  ! Compute dt and number of timesteps
  ntsteps = ceiling(tfinal / dt)
  dt = tfinal / ntsteps
 ! write(*,*) 'dt = ', dt

  ! Below we will compute 
  ! f_{rr} + (1/r) * f_r + (1/r^2) * f_{\theta \theta} = rhs
  ! And then solve 
  ! u_{rr} + (1/r) * u_r + (1/r^2) * u_{\theta \theta} = rhs
  ! with zero Dirichlet bc.
  ! After Fourier transform the second equation becomes 
  ! v(k)_{rr} + (1/r) * v(k)_r - (k^2/r^2) * v(k) = FFT[rhs](k)
  ! In matrix form we have
  ! DMRR + diag(1/r) * DMR -k^2*diag(1/r^2)
  ! We store the k independent part in Amatrix
  amatrix1 = sigs * dmrr1(2:nr1-1,2:nr1-1) 
  do i1 = 1,nr1-2
   amatrix1(i1,:) = amatrix1(i1,:) + &
       sigs*dmr1(i1+1,2:nr1-1)/r1(i1+1) + dmr1(i1+1,2:nr1-1)*r1(i1+1)
   amatrix1(i1,i1) = amatrix1(i1,i1) + 2.d0
  end do
  amatrix2 = sigs * dmrr2(2:nr2-1,2:nr2-1) 
  do i2 = 1,nr2-2
   amatrix2(i2,:) = amatrix2(i2,:) + &
       sigs*dmr2(i2+1,2:nr2-1)/r2(i2+1) + dmr2(i2+1,2:nr2-1)*r2(i2+1)
   amatrix2(i2,i2) = amatrix2(i2,i2) + 2.d0
  end do

  do j2 = 1,nthetah+1
  bmatrix2 = dt* amatrix2

    do i2 = 1,nr2-2
       bmatrix2(i2,i2) =  bmatrix2(i2,i2) - & 
                                  dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j2) &
                                     +sigs*P_A(dm)*k(j2)**2/r2(i2+1)**2) 
    end do
    do j1 = 1,nthetah+1
     bmatrix1 = dt  * amatrix1
     do i1 = 1,nr1-2
       bmatrix1(i1,i1) =  bmatrix1(i1,i1) - & 
                                  dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j1) &
                                     +sigs*P_A(dm)*k(j1)**2/r1(i1+1)**2) 
     end do



      start = 0
      do i2 = 1,nr2-2
       do i1 = 1,nr1-2
        colptr(i1+(i2-1)*(nr1-2)) = start
        ! Second matrix upper triangular
        if (i2 > 1) then
         values(start+1:start+(i2-1),j1,j2) = bmatrix2(1:(i2-1),i2)
         do i = 1,(i2-1)
          rowind(start+i) = i1+(i-1)*(nr1-2)-1
         enddo
         start = start+i2-1
        endif
        ! First  matrix upper triangular
        if (i1 > 1) then
         values(start+1:start+(i1-1),j1,j2) = bmatrix1(1:(i1-1),i1)
         do i = 1,(i1-1)
          rowind(start+i) = i+(i2-1)*(nr1-2)-1
         enddo
         start = start+i1-1
        endif
        ! Diagonal
        values(start+1,j1,j2) = bmatrix1(i1,i1) + bmatrix2(i2,i2) - 1.5d0
        rowind(start+1) = i1+(i2-1)*(nr1-2) - 1
        start = start+1
        ! First  matrix lower triangular
        if (i1 < nr1-2) then
         values(start+1:start+(nr1-2-i1),j1,j2) = bmatrix1(i1+1:nr1-2,i1)
         do i = 1,nr1-2-i1
          rowind(start+i) = i1+i+(i2-1)*(nr1-2)-1
         enddo
         start = start+(nr1-2)-i1;
        endif
        ! Second matrix lower triangular
        if (i2 < nr2-2) then
         values(start+1:start+(nr2-2-i2),j1,j2) = bmatrix2(i2+1:nr2-2,i2)
         do i = 1,nr2-2-i2
          rowind(start+i) = i1+(i+i2-1)*(nr1-2)-1
         enddo
         start = start+(nr2-2)-i2;
        endif
       enddo
      enddo
      write(*,*) start-nnz
      colptr((nr1-2)*(nr2-2)+1) = start

      !  write(*,*) rowind(1:(nr-2)*(nr-2))

      rowind = rowind+1
      colptr = colptr+1

      open(1,file=trim('row.txt'),status='unknown')
      write(1,*) rowind(1:nnz)
      open(3,file=trim('col.txt'),status='unknown')
      write(3,*) colptr(1:n+1)
      open(4,file=trim('values.txt'),status='unknown')
      write(4,*) realpart(values(1:nnz,j1,j2))
      close(1)
      close(3)
      close(4)
     ! stop

      !  write(*,*) start-nnz
      values(:,j1,j2) = -values(:,j1,j2)
      call c_fortran_zgssv(1,n,nnz,1,values(:,j1,j2),rowind,colptr,b,ldb,factors(j1,j2),&
           info)
      if (info .eq. 0) then
      !    write(*,*) 'Factorization succeeded'
      else  
       write(*,*)'INFO from fact=',info
      endif
    enddo
   enddo



  ! Omega vector
  om(:,:,:,:,:) = 0.d0
  u(:,:,:,:,:) = 0.d0
  do j2 = 1,ntheta
    do i2 = 1,nrh2
      om(:,i2,:,j2,3) = 1.d0! 10.d0+2*pi*0.1d0*((x+y)+(x(i,j)+y(i,j)))
  ! The initial coundition, we set 

      u(:,i2,:,j2,1) = exp(-(x1**2 + y1**2) / (2.d0*sigs))              /(2.d0*pi*sigs) * &
                       exp(-(x2(i2,j2)**2 + y2(i2,j2)**2) / (2.d0*sigs))/(2.d0*pi*sigs)! &
                            ! * (1.d0 + x**2) / (1.d0 + sigs)
      u(:,i2,:,j2,2) = exp(-(x1**2 + y1**2) / (2.d0*sigs))              /(2.d0*pi*sigs) * &
                       exp(-(x2(i2,j2)**2 + y2(i2,j2)**2) / (2.d0*sigs))/(2.d0*pi*sigs)! &
                            ! * (1.d0 + x**2) / (1.d0 + sigs)
      u(:,i2,:,j2,3) = exp(-(x1**2 + y1**2) / (2.d0*sigs))              /(2.d0*pi*sigs) * &
                       exp(-(x2(i2,j2)**2 + y2(i2,j2)**2) / (2.d0*sigs))/(2.d0*pi*sigs)! &
                            ! * (1.d0 + x**2) / (1.d0 + sigs)
    enddo
  enddo
  !uex(:,:,:,:,3) = u(:,:,:,:,3)

  write(*,*) 'Timestep ', 1, '/', ntsteps
  do dm = 1,dims
   do i2 = 1,nrh2
    do i1 = 1,nrh1
     ! Transform the solution
     u_r = u(i1,i2,:,:,dm)
     call dfftw_execute (forward)
     uhat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1)
     ! Transform the coupling term
     call getCouplingTerm(u(i1,i2,:,:,:),om(i1,i2,:,:,:),u_r,ntheta,dm)
     call dfftw_execute (forward)
     ohat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1)
    enddo
   enddo
  enddo
  ohatp = ohat
  uhatp = uhat


  u(:,:,:,:,1) = (cos(dt) - sin(dt))*u(:,:,:,:,3)
  u(:,:,:,:,2) = (sin(dt) + cos(dt))*u(:,:,:,:,3)

  do j2 = 1,ntheta
    do i2 = 1,nrh2
      uex(:,i2,:,j2,3) = exp(-(x1**2 + y1**2) / (2.d0*sigs))/(2.d0*pi*sigs) * &
                         exp(-(x2(i2,j2)**2 + y2(i2,j2)**2) / (2.d0*sigs))/(2.d0*pi*sigs)! &
    enddo
  enddo

  uex(:,:,:,:,1) = (cos(tfinal) - sin(tfinal))*uex(:,:,:,:,3)
  uex(:,:,:,:,2) = (sin(tfinal) + cos(tfinal))*uex(:,:,:,:,3)



 ! uex(:,:,:,:,1) = (cos(3*dt) - sin(3*dt))*u(:,:,:,:,3)
 ! uex(:,:,:,:,2) = (sin(3*dt) + cos(3*dt))*u(:,:,:,:,3)
 ! uex(:,:,:,:,3) = u(:,:,:,:,3)

!  do dm = 1,dims
!   do j2 = 1,ntheta
!   do j1 = 1,ntheta
!   do i2 = 1,nrh
!    uout(:,i2,j1,j2) = u(:,i2,j1,j2,dm)*rh
!   enddo
!   enddo
!   enddo
!   write(filename,'("u",I1.1,"_",I8.8,".txt")') dm, 0
!   call printdble2d(uout,1,nrh,1,ntheta,trim(filename))
!  enddo
  
  
  ! Proceed with multistep
  ! Now solve 
  !(I - 2/3dtL_{FP})u^{nu+1}  = 4/3u^{nu} - 1/3u^{nu} 
  !                           + 4/3dt * (omega \cross u^{nu} - omega \cross u^{nu-1})
  ! Go to wave number space 
  do ts = 2, ntsteps
   write(*,*) 'Timestep ', ts, '/', ntsteps
   do dm = 1,dims
    do i2 = 1,nrh2
     do i1 = 1,nrh1
      ! Transform the solution
      u_r = u(i1,i2,:,:,dm)
      call dfftw_execute (forward)
      uhat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1)
      ! Transform the coupling term
      call getCouplingTerm(u(i1,i2,:,:,:),om(i1,i2,:,:,:),u_r,ntheta,dm)
      call dfftw_execute (forward)
      ohat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1)
     enddo
    enddo
   enddo
   fhat =(2.d0*uhat - 0.5d0 * uhatp + 2.d0*dt*ohat - dt*ohatp)
   ohatp(:,:,:,:,:) = ohat(:,:,:,:,:)
   uhatp(:,:,:,:,:) = uhat(:,:,:,:,:)

   do dm = 1,dims
    ! Now invert for one mode at a time
    what = 0.d0
    do j2 = 1,nthetah+1
     do j1 = 1,nthetah+1

      !Construct right hand side
      do i2 = 1,nrh2
       do i1 = 1,nrh1
         w_c(     i1,     i2) = fhat(nrh1+1-i1,nrh2+1-i2,j1,j2,dm)
         w_c(nrh1+i1,     i2) = fhat(       i1,nrh2+1-i2,j1,j2,dm)
         w_c(     i1,nrh2+i2) = fhat(nrh1+1-i1,       i2,j1,j2,dm)
         w_c(nrh1+i1,nrh2+i2) = fhat(       i1,       i2,j1,j2,dm)
       end do
      enddo
      rhs(1:nr1-2,1:nr2-2) = w_c(2:nr1-1,2:nr2-1)
      b(1:n) = reshape(rhs,(/(nr1-2)*(nr2-2)/))

      ! Factor

   !   write(*,*) n, nnz, ldb
   !   write(*,*) colptr(1:n+1)
   !   write(*,*) rowind(1:nnz)


      ! Solve
      call c_fortran_zgssv(2,n,nnz,1,values(:,j1,j2),rowind,colptr,b,ldb,factors(j1,j2),info)
      if (info .eq. 0) then
  !     write (*,*) 'Solve succeeded'
      ! write (*,*) (b(i), i=1, 10)
      else
       write(*,*) 'INFO from triangular solve = ', info
      endif

      
       w_c = 0.d0
       w_c(2:nr1-1,2:nr2-1) = reshape(b(1:n),(/nr1-2,nr2-2/)) 

      do i2 = 1,nrh2
       do i1 = 1,nrh1
        what(nrh1+1-i1,nrh2+1-i2,j1,j2) = w_c(     i1,     i2)
        what(       i1,nrh2+1-i2,j1,j2) = w_c(nrh1+i1,     i2) 
        what(nrh1+1-i1,       i2,j1,j2) = w_c(     i1,nrh2+i2)
        what(       i1,       i2,j1,j2) = w_c(nrh1+i1,nrh2+i2) 
       enddo
      enddo
     enddo
    enddo
    do i2 = 1,nrh2
     do i1 = 1,nrh1
      u_c(:,1:nthetah+1) = what(i1,i2,:,:)
      u_c(:,nthetah+2:ntheta) = what(i1,i2,:,2:nthetah)
      call dfftw_execute (backward)
      u(i1,i2,:,:,dm) = u_r/(dble(ntheta)**2)
     enddo 
    enddo
 !   uex(:,:,:,:,1) = (cos(dt*ts) - sin(dt*ts))*uex(:,:,:,:,3)
 !   uex(:,:,:,:,2) = (sin(dt*ts) + cos(dt*ts))*uex(:,:,:,:,3)
    write(filename,'("u",I1.1,"_",I8.8,".txt")') dm, ts
    call printdble2d(u(:,1,:,1,dm),1,nrh1,1,ntheta,trim(filename))
 !   write(filename,'("e",I1.1,"_",I8.8,".txt")') dm, ts
 !   call printdble2d(u(:,1,:,1,dm)-uex(:,1,:,1,dm),1,nrh,1,ntheta,trim(filename))
   enddo
  enddo

 ! call printdble2d(x,1,nrh,1,ntheta,'x.txt')
!  call printdble2d(y,1,nrh,1,ntheta,'y.txt')

  
  
 ! call clenshaw_curtis_compute(nr,xcc,wcc)
 ! call rescale(-rmax,rmax,nr,xcc,wcc)
 ! do dm = 1,dims
 ! do j2 = 1,ntheta
 ! do j1 = 1,ntheta
 ! do i1 = 1,nrh
 !   u(i1,1,j1,j2,dm) = 2*sum(wcc(nrh/2+1:nr)*u(i1,1:nrh,j1,j2,dm))
 ! enddo
 ! enddo
 ! enddo
 ! enddo

  
 ! write(2,*) nr1, maxval(abs(uex(:,:,:,:,:)-u(:,:,:,:,:)))
 ! write(*,*) nr1, maxval(abs(uex(:,:,:,:,:)-u(:,:,:,:,:)))

  
  DEALLOCATE(u,f,om,xcc,wcc)
  deallocate(up,uex,uout)
  DEALLOCATE(x1,y1,r1,theta,rh1)
  DEALLOCATE(x2,y2,r2,rh2)
  DEALLOCATE(dmrr1,dmr1,dm_coeff1)
  DEALLOCATE(dmrr2,dmr2,dm_coeff2)
  deallocate(fhat,uhat,fhatp,uhatp)
  deallocate(ohatp,ohat, what, w_c)
  deallocate(amatrix1,amatrix2,bmatrix1,bmatrix2,rhs)

  do j2 = 1,nthetah+1
  do j1 = 1,nthetah+1
      ! Free superlu 
      call c_fortran_zgssv(3,n,nnz,1,values(:,j1,j2),rowind,colptr,b,ldb,factors(j1,j2),info)
      enddo
      enddo
  ! 
  call destroyfft

  enddo

  close(2)
  deallocate(b,values)
  !   
end program MAIN

subroutine printdble2d(u,nx1,nx2,ny1,ny2,str)
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, intent(in) :: nx1,nx2,ny1,ny2
  real(dp), intent(in) :: u(nx1:nx2,ny1:ny2)
  character(len=*), intent(in) :: str
  integer :: i,j
  open(2,file=trim(str),status='unknown')
  do j=ny1,ny2,1
   do i=nx1,nx2,1
    if(abs(u(i,j)) .lt. 1e-40) then
     write(2,fmt='(E24.16)',advance='no') 0.d0
    else
     write(2,fmt='(E24.16)',advance='no') u(i,j)
    end if
   end do
   write(2,'()')
  end do
  close(2)
end subroutine printdble2d

subroutine getCouplingTerm(u,om,ct,n,dm)
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, intent(in) :: n,dm
  real(dp), intent(in) :: u(n,n,3)
  real(dp), intent(in) :: om(n,n,3)
  real(dp), intent(out) :: ct(n,n)

  if (dm == 1) then
     ct = (om(:,:,2)*u(:,:,3) - om(:,:,3)*u(:,:,2))
    elseif (dm == 2) then
     ct = (om(:,:,3)*u(:,:,1) - om(:,:,1)*u(:,:,3))
    else
     ct = (om(:,:,1)*u(:,:,2) - om(:,:,2)*u(:,:,1))
  endif
end subroutine getCouplingTerm

!  ! Now start the timestepping
!  ! First timestep to preheat BDF scheme 
!  ! Second order explicit RK
!  ! k1 = hf(u0)
!  ! k2 = hf(u0 + 1/2k1)
!  ! u1 = u0 + k2,
!  ! Go to wave number space 
!  do ts = 1,1
!  do dm = 1,dims
!   do i2 = 1,nrh
!    do i1 = 1,nrh
!    ! Transform the solution
!     u_r = u(i1,i2,:,:,dm)
!     call dfftw_execute (forward)
!     uhat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1)
!    ! Transform the coupling term
!     call getCouplingTerm(u(i1,i2,:,:,:),om(i1,i2,:,:,:),u_r,ntheta,dm)
!     call dfftw_execute (forward)
!     ohatp(i1,i2,:,:,dm) = u_c(:,1:nthetah+1)
!    enddo
!   enddo
!  enddo
!
!  uhatp(:,:,:,:,:) = uhat(:,:,:,:,:)
!
!  do dm = 1,dims
!   ! Now invert for one mode at a time
!   what = 0.d0
!   do j2 = 1,nthetah+1
!    bmatrix2 = dt * P_A(dm) * amatrix
!    do i2 = 1,nr-2
!       bmatrix2(i2,i2) =  bmatrix2(i2,i2) - & 
!                                  dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j2) &
!                                     +sigs*P_A(dm)*k(j2)**2/r(i2+1)**2) 
!    end do
!    do j1 = 1,nthetah+1
!     bmatrix1 = dt * P_A(dm) * amatrix
!     do i1 = 1,nr-2
!       bmatrix1(i1,i1) =  bmatrix1(i1,i1) - & 
!                                  dt*(P_B(dm)*complex(0.d0, 1.d0)*k(j1) &
!                                     +sigs*P_A(dm)*k(j1)**2/r(i1+1)**2) 
!     end do
!
!
!     do i2 = 1,nrh
!      do i1 = 1,nrh
!       w_c(    i1,    i2) = uhat(nrh+1-i1,nrh+1-i2,j1,j2,dm)
!       w_c(nrh+i1,    i2) = uhat(      i1,nrh+1-i2,j1,j2,dm)
!       w_c(    i1,nrh+i2) = uhat(nrh+1-i1,      i2,j1,j2,dm)
!       w_c(nrh+i1,nrh+i2) = uhat(      i1,      i2,j1,j2,dm)
!      end do
!     enddo
!     do i2 = 1,nr-2
!      rhs(1:nr-2,i2) = MATMUL(bmatrix1, w_c(2:nr-1,i2))
!     enddo
!     do i1 = 1,nr-2
!      rhs(i1,1:nr-2) = rhs(i1,1:nr-2) + MATMUL(bmatrix2, w_c(i1,2:nr-1))
!     enddo
!     do i2 = 1,nrh
!      do i1 = 1,nrh
!       w_c(    i1,    i2) = ohatp(nrh+1-i1,nrh+1-i2,j1,j2,dm)
!       w_c(nrh+i1,    i2) = ohatp(      i1,nrh+1-i2,j1,j2,dm)
!       w_c(    i1,nrh+i2) = ohatp(nrh+1-i1,      i2,j1,j2,dm)
!       w_c(nrh+i1,nrh+i2) = ohatp(      i1,      i2,j1,j2,dm)
!      end do
!     enddo
!     rhs(1:nr-2,1:nr-2) = rhs(1:nr-2,1:nr-2) + dt*w_c(2:nr-1,2:nr-1)
!     w_c = 0.d0
!     w_c(2:nr-1,2:nr-1) = rhs(1:nr-2,1:nr-2) 
!     do i2 = 1,nrh
!      do i1 = 1,nrh
!       what(nrh+1-i1,nrh+1-i2,j1,j2) = w_c(    i1,    i2)
!       what(      i1,nrh+1-i2,j1,j2) = w_c(nrh+i1,    i2) 
!       what(nrh+1-i1,      i2,j1,j2) = w_c(    i1,nrh+i2)
!       what(      i1,      i2,j1,j2) = w_c(nrh+i1,nrh+i2) 
!      enddo
!     enddo
!    enddo 
!   enddo
!   do i2 = 1,nrh
!    do i1 = 1,nrh
!       u_c(:,1:nthetah+1) = what(i1,i2,:,:)
!       call dfftw_execute (backward)
!       up(i1,i2,:,:,dm) = u_r/(dble(ntheta)**2)
!    enddo
!   enddo
!  enddo
!  
!
!  do dm = 1,dims
!   do i2 = 1,nrh
!    do i1 = 1,nrh
!    ! Transform the solution
!     u_r = u(i1,i2,:,:,dm)+0.5d0*up(i1,i2,:,:,dm)
!     call dfftw_execute (forward)
!     uhat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1) 
!    ! Transform the coupling term
!     call getCouplingTerm(u(i1,i2,:,:,:)+0.5d0*up(i1,i2,:,:,:),om(i1,i2,:,:,:),u_r,ntheta,dm)
!     call dfftw_execute (forward)
!     fhat(i1,i2,:,:,dm) = u_c(:,1:nthetah+1) 
!    enddo
!   enddo
!  enddo
!
!  do dm = 1,dims
!   ! Now invert for one mode at a time
!   what = 0.d0
!   
!    do j2 = 1,nthetah+1
!    do j1 = 1,nthetah+1
!
!     do i2 = 1,nrh
!      do i1 = 1,nrh
!       w_c(    i1,    i2) = uhat(nrh+1-i1,nrh+1-i2,j1,j2,dm)
!       w_c(nrh+i1,    i2) = uhat(      i1,nrh+1-i2,j1,j2,dm)
!       w_c(    i1,nrh+i2) = uhat(nrh+1-i1,      i2,j1,j2,dm)
!       w_c(nrh+i1,nrh+i2) = uhat(      i1,      i2,j1,j2,dm)
!      end do
!     enddo
!     do i2 = 1,nr-2
!      rhs(1:nr-2,i2) = MATMUL(bmatrix1, w_c(2:nr-1,i2))
!     enddo
!     do i1 = 1,nr-2
!      rhs(i1,1:nr-2) = rhs(i1,1:nr-2) + MATMUL(bmatrix2, w_c(i1,2:nr-1))
!     enddo
!     do i2 = 1,nrh
!      do i1 = 1,nrh
!       w_c(    i1,    i2) = fhat(nrh+1-i1,nrh+1-i2,j1,j2,dm)
!       w_c(nrh+i1,    i2) = fhat(      i1,nrh+1-i2,j1,j2,dm)
!       w_c(    i1,nrh+i2) = fhat(nrh+1-i1,      i2,j1,j2,dm)
!       w_c(nrh+i1,nrh+i2) = fhat(      i1,      i2,j1,j2,dm)
!      end do
!     enddo
!     rhs(1:nr-2,1:nr-2) = rhs(1:nr-2,1:nr-2) + dt*w_c(2:nr-1,2:nr-1)
!     w_c = 0.d0
!     w_c(2:nr-1,2:nr-1) = rhs(1:nr-2,1:nr-2) 
!     do i2 = 1,nrh
!      do i1 = 1,nrh
!       what(nrh+1-i1,nrh+1-i2,j1,j2) = w_c(    i1,    i2)
!       what(      i1,nrh+1-i2,j1,j2) = w_c(nrh+i1,    i2) 
!       what(nrh+1-i1,      i2,j1,j2) = w_c(    i1,nrh+i2)
!       what(      i1,      i2,j1,j2) = w_c(nrh+i1,nrh+i2) 
!      enddo
!     enddo
!    enddo
!   enddo
!   do i2 = 1,nrh
!    do i1 = 1,nrh
!     u_c(:,1:nthetah+1) = what(i1,i2,:,:)
!     call dfftw_execute (backward)
!     u(i1,i2,:,:,dm) = u(i1,i2,:,:,dm) + u_r/(dble(ntheta)**2)
!    enddo 
!   enddo
!  enddo
!  enddo


subroutine clenshaw_curtis_compute (order,x,w)
!
  implicit none

  integer ( kind = 4 ) order

  real ( kind = 8 ) b
  integer ( kind = 4 ) i
  integer ( kind = 4 ) j
  real ( kind = 8 ), parameter :: r8_pi = 3.141592653589793D+00
  real ( kind = 8 ) theta
  real ( kind = 8 ) w(order)
  real ( kind = 8 ) x(order)

  if ( order < 1 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'CLENSHAW_CURTIS_COMPUTE - Fatal error!'
    write ( *, '(a,i8)' ) '  Illegal value of ORDER = ', order
    stop
  end if

  if ( order == 1 ) then
    x(1) = 0.0D+00
    w(1) = 2.0D+00
    return
  end if

  do i = 1, order
    x(i) = cos ( real ( order - i, kind = 8 ) * r8_pi &
               / real ( order - 1, kind = 8 ) )
  end do

  x(1) = -1.0D+00
  if ( mod ( order, 2 ) == 1 ) then
    x((order+1)/2) = 0.0D+00
  end if
  x(order) = +1.0D+00

  do i = 1, order

    theta = real ( i - 1, kind = 8 ) * r8_pi &
          / real ( order - 1, kind = 8 )

    w(i) = 1.0D+00

    do j = 1, ( order - 1 ) / 2

      if ( 2 * j == ( order - 1 ) ) then
        b = 1.0D+00
      else
        b = 2.0D+00
      end if

      w(i) = w(i) - b * cos ( 2.0D+00 * real ( j, kind = 8 ) * theta ) &
           / real ( 4 * j * j - 1, kind = 8 )

    end do

  end do

  w(1)         =           w(1)         / real ( order - 1, kind = 8 )
  w(2:order-1) = 2.0D+00 * w(2:order-1) / real ( order - 1, kind = 8 )
  w(order)     =           w(order)     / real ( order - 1, kind = 8 )

  return
end subroutine clenshaw_curtis_compute


subroutine rescale ( a, b, n, x, w )
  implicit none

  integer ( kind = 4 ) n

  real ( kind = 8 ) a
  real ( kind = 8 ) b
  real ( kind = 8 ) w(n)
  real ( kind = 8 ) x(n)

  x(1:n) = ( ( a + b ) + ( b - a ) * x(1:n) ) / 2.0D+00
  w(1:n) = ( b - a ) * w(1:n) / 2.0D+00

  return
end
