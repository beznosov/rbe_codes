clear

nthetah = 5;
nrh = 32;
ntheta = nthetah*2;
nr = nrh*2;
rmax = 8;
ls1 = dir('u1*.txt');
ls2 = dir('u2*.txt');
ls3 = dir('u3*.txt');

t = (0:length(ls1)-1)*0.001;

figure(2)
clf
I1 = [];
I2 = I1;
I3 = I2;
for fl = 1:length(ls1)
u = load(ls1(fl).name);
u = u/(2*(nr-1));
I = 0.0;
for it = 1:nthetah
    I = I + ...
    rmax*pi/nthetah*...
    clenshaw_curtis([u(it, end:-1:1)'; u(it+nthetah, 1:end)'], nr-1);
end
I1 = [I1; I];
u = load(ls2(fl).name);
u = u/(2*(nr-1));
I = 0.0;
for it = 1:nthetah
    I = I + ...
    rmax*pi/nthetah*...
    clenshaw_curtis([u(it, end:-1:1)'; u(it+nthetah, 1:end)'], nr-1);
end
I2 = [I2; I];
u = load(ls3(fl).name);
u = u/(2*(nr-1));
I = 0.0;
for it = 1:nthetah
    I = I + ...
    rmax*pi/nthetah*...
    clenshaw_curtis([u(it, end:-1:1)'; u(it+nthetah, 1:end)'], nr-1);
end
I3 = [I3; I];
%
%u1 = load(ls2(fl).name);
%U    = [u1; u1(1,:)];
%contour(X,Y,U, 30)
%caxis([0 1e-3])
%hold on
%contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
%colorbar
%set(gca,'fontsize',18)
%axis equal
%hold off
%drawnow
%subplot(1,3,3);
%u1 = load(ls3(fl).name);
%U    = [u1; u1(1,:)];
%contour(X,Y,U, 30)
%caxis([0 1e-3])
%hold on
%contour(X,Y,X.^2+Y.^2,[0.99 0.99],'k')
%colorbar
%set(gca,'fontsize',18)
%axis equal
%hold off
end

%plot(t,I1,'-k',t,I2,'-r',t,I3,'-b','linewidth',2);
semilogy(t,sqrt(I1.^2+I2.^2+I3.^2),'-b','linewidth',2);
xlabel('\theta')
ylabel('I_n')
legend('I_1','I_2','I_3');
